"""
Uses: conda activate birds
"""

import pandas as pd
import openpyxl
import argparse
from io import StringIO
# from icrawler.builtin import GoogleImageCrawler <-- doesn't work anymore
from linkpreview import link_preview
import urllib.request
from PIL import Image
from docx import Document
from docx.shared import Inches
from docx.enum.section import WD_ORIENT
import os
import glob
import logging

TMP_BIRD_PATH = "./bird_images/"

def _get_link_if_exists(cell) -> str | None:
    try:
        return cell.hyperlink.target
    except AttributeError:
        return None

# taken from https://gist.github.com/zachschillaci27/887c272cbdda63316d9a02a770d15040
# format expected is link in Morocco.xlsx (one col is Bird, the other is same but with header Link)
def read_birds(
    file_name: str, sheet_name: str = "full data", columns_to_parse: list[str] = ["Link"], row_header: int = 1
) -> pd.DataFrame:
    df = pd.read_excel(file_name, sheet_name)
    ws = openpyxl.load_workbook(file_name)[sheet_name]
    for column in columns_to_parse:
        row_offset = row_header + 1
        column_index = list(df.columns).index(column) + 1
        df[column] = [
            _get_link_if_exists(ws.cell(row=row_offset + i, column=column_index))
            for i in range(len(df[column]))
        ]
    return df


def get_bird_image(bird, image_url):
    bird_filedir = TMP_BIRD_PATH + bird
    bird_filename = bird_filedir  + '/000001.png'
    if not os.path.isfile(bird_filedir):
        os.makedirs(bird_filedir, exist_ok=True)
        # get image from default preview image
        preview = link_preview(image_url)
        if preview.image is not None:
            urllib.request.urlretrieve(preview.image, bird_filename)
        else:
            logging.info ("Skipping bird %s as no image preview found" % (bird))
    else:
        logging.info("Skipping bird %s as image already exists in our mini db of birds" % (bird))

if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description="Generates a docx of birds from a txt file. Uses conda env named birds. \n Example usage: python birds.py --file Kruger_birds_short.txt --output 'demo_birds_c.docx' # remember to conda activate birds")
    parser.add_argument('--file', required=True, action='store', default=4, help='txt file to parse with __ as denominator')
    parser.add_argument('--output',required=True, action='store', default=4, help='word docx to save') 
    args = parser.parse_args()
    output = args.output

    # expected result is 2 col dataframe. col 1 is "Bird"; col 2 is "Link" linking to bird image
    birds = read_birds(args.file)

    # Create word document
    document = Document()
    section = document.sections[-1]
    section.orientation = WD_ORIENT.LANDSCAPE
    new_width, new_height = section.page_height, section.page_width
    section.page_width = new_width
    section.page_height = new_height

    row_idx = 0
    col_idx = 0

    # For every bird in the csv, download the bird image and add it to the word doc in the next cell in the table
    for idx,row in birds.iterrows():
        
        # some bird names have a "/" in the name which messes up my path names
        bird = row['Bird'].strip().replace("/", " ")
        logging.info ("Starting on %s" % bird)
        
        # download bird image and save it to file path with bird name and 000001.jpg
        if row["Link"] is not None:
            get_bird_image(bird, row["Link"])

        # add a new table if we have no more space
        if (idx % 12) == 0:
            document.add_page_break()
            cur_table = document.add_table(rows=3, cols=4)
            cur_table.style = 'Table Grid'
            row_idx = 0
            col_idx = 0

        # reset row and col index if needed
        if col_idx >= 4:
            col_idx = 0
            row_idx = row_idx + 1
        if row_idx >= 3:
            row_idx = 0
    
        # get next cell in table and add in the text of this bird
        cell_paragraph = cur_table.cell(row_idx,col_idx).paragraphs[0]
        cell_paragraph.text = bird

        try:
            # drop in the picture underneath the name (note that table have autofit) 
            # Note: I had to change /Users/kina/devwork/miniconda3/envs/birds/lib/python3.8/site-packages/docx/image/tiff.py bc it was throwing a rounding error if the image didn't have a dpi res default or something
            # TODO: this is supposed to compress the images down w height arg, but the resulting doc size is still huge. I think the scaling (which I did have to fix -- see above) is off.
    
            if os.path.isdir(TMP_BIRD_PATH + bird):
                filepath = glob.glob(TMP_BIRD_PATH + bird + "/000001.*")[0] # sometimes image is a png or jpg
                cell_paragraph.add_run().add_picture(filepath, height=Inches(2))
        except:
            try:
                # try with the second picture too
                filepath = TMP_BIRD_PATH + bird + "/000002.jpg"
                if os.path.isfile(filepath):
                    cell_paragraph.add_run().add_picture(filepath, height=Inches(2))
            except:
                logging.error ("ERROR with writing image with width = 2in: %s" % (TMP_BIRD_PATH + bird + "/000001.jpg" or + "/000002.jpg"))
            # breakpoint()            

        col_idx = col_idx + 1
        
# Change the page margins (not sure if I have to do this with all the sections or maybe could have used -1 index?)
sections = document.sections
for section in sections:
    section.top_margin = Inches(0.5)
    section.bottom_margin = Inches(0.5)
    section.left_margin = Inches(0.5)
    section.right_margin = Inches(0.5)

# save doc
if not output.endswith(".docx"):
    output = output + ".docx"
document.save(output)
