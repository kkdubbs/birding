# birding

Create a word doc of birds from a xslx file containing links to bird images

```
conda activate birds
python birds.py --file Morroco.xlsx --output 'Morroco.docx'

```

# dependencies
```
pip list
Package            Version
------------------ ------------
beautifulsoup4     4.12.2
bs4                0.0.1
certifi            2023.7.22
charset-normalizer 3.2.0
icrawler           0.6.7
idna               3.4
lxml               4.9.3
numpy              1.24.4
pandas             2.0.3
Pillow             10.0.0
pip                23.2.1
python-dateutil    2.8.2
python-docx        0.8.11
pytz               2023.3.post1
PyYAML             6.0.1
requests           2.31.0
setuptools         68.0.0
simplejson         3.19.1
six                1.16.0
soupsieve          2.5
tzdata             2023.3
urllib3            2.0.4
wheel              0.38.4
```